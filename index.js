/// <reference path="index.d.ts" />

import { promisifyAudio } from './lib/audio';

module.exports = {
  promisifyAudio,
};
/**
 * Download audio from server
 * @param {string} link Link to download your audio
 * @returns {Promise<HTMLAudioElement>} Your audio element
 */
export function promisifyAudio(link) {
  return new Promise((res, rej) => {
    const audio = new Audio(link);
    
    const onResolve = () => {
      clearListeners();
      return res(audio);
    };

    const onReject = (e) => {
      clearListeners();
      return rej(e);
    };

    const clearListeners = () => {
      audio.removeEventListener('error', onReject);
      audio.removeEventListener('canplay', onResolve);
      audio.removeEventListener('canplaythrough', onResolve);
    };

    audio.addEventListener('canplay', onResolve);
    audio.addEventListener('canplaythrough', onResolve);
    audio.addEventListener('error', onReject);
  });
}